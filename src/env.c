/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "env.h"
#include "log.h"

struct env_save_bashrc_data_s {
    char * aclocal;
    size_t aclocal_len;
    char * bins;
    size_t bins_len;
    char * libs;
    size_t libs_len;
    char * pkgconfig;
    size_t pkgconfig_len;
};
typedef struct env_save_bashrc_data_s env_save_bashrc_data_t;

struct env_execvar_str_s {
    struct env_execvar_str_s * prev;
    struct env_execvar_str_s * next;
    size_t len;
    char   str[];
};
typedef struct env_execvar_str_s env_execvar_str_t;

struct env_execvar_s {
    env_execvar_str_t *  head;
    env_execvar_str_t *  tail;
    size_t               tail_arr_siz;
    env_execvar_str_t ** tail_arr; /* sorted array */

    size_t               nstr;
    size_t               tlen;

    char *               result;
};
typedef struct env_execvar_s env_execvar_t;

struct env_exec_data_s {
    env_execvar_t aclocal;
    env_execvar_t bins;
    env_execvar_t libs;
    env_execvar_t pkgconfig;
};
typedef struct env_exec_data_s env_exec_data_t;

struct env_prop_s {
    const char * name;
    int          flag;
};
typedef struct env_prop_s env_prop_t;

static const env_prop_t env_props[] = {
    {"aclocal",   ENV_PREFIX_ACLOCAL},
    {"bins",      ENV_PREFIX_BINS},
    {"enable",    ENV_PREFIX_ENABLE},
    {"export",    ENV_PREFIX_EXPORT},
    {"install",   ENV_PREFIX_INSTALL},
    {"libs",      ENV_PREFIX_LIBS},
    {"pkgconfig", ENV_PREFIX_PKGCONFIG},
    {"sbins",     ENV_PREFIX_SBINS}
};

static inline int NONNULL
env_flag_ynq(int * flags, int flag, env_ynq_t ynq)
{
    switch (ynq) {
        case ENV_YES:
            (*flags) |= flag;
            return 0;
        case ENV_NO:
            (*flags) &= ~flag;
            return 0;
        case ENV_QUERY:
            return (*flags) & flag;
    }
never:
    COLD UNUSED;
    abort();
}

static char * NONNULL
env_copy_string(const char * string, size_t * lenp)
{
    char * copy;
    *lenp = strlen(string);
    copy = malloc(*lenp + 1);
    if (copy == NULL) {
        log_error_errno("could not allocate memory");
        return NULL;
    }
    memcpy(copy, string, *lenp + 1);
    return copy;
}

static char * NONNULL
env_default_root(size_t * lenp)
{
    const char * le_root = getenv("KE_ROOT");
    const char * home    = getenv("HOME");
    if (le_root != NULL) {
        return env_copy_string(le_root, lenp);
    } else if (home != NULL) {
        size_t home_len;
        size_t package_len;
        char * root;

        home_len = strlen(home);
        package_len = strlen(PACKAGE);
        *lenp = home_len + package_len + 1;
        root = malloc(*lenp + 1);
        if (root == NULL) {
            log_error_errno("could not allocate memory");
            return NULL;
        }
        memcpy(root, home, home_len);
        root[home_len] = '/';
        memcpy(root + home_len + 1, PACKAGE, package_len + 1);
        return root;
    } else {
        log_warn("neither ${KE_ROOT} nor ${HOME} set");
        return env_copy_string(PACKAGE, lenp);
    }
}

static int NONNULL
env_remove_recursive_fn(const char * fpath,
                        const struct stat * sb,
                        int typeflag,
                        struct FTW * ftwbuf)
{
    return remove(fpath);
}

static int NONNULL
env_remove_recursive(const char * path)
{
    /* 32 should do just fine here and it is reasonably small so
     * hopefully nftw() will not run into ENFILE. */
    if (nftw(path, &env_remove_recursive_fn, 32, FTW_DEPTH)) {
        log_error_errno("could not remove %s", path);
        return 1;
    }
    return 0;
}

static int NONNULL
env_prop_cmp(const void * p1, const void * p2)
{
    const env_prop_t * prop1 = p1;
    const env_prop_t * prop2 = p2;
    return strcmp(prop1->name, prop2->name);
}

void NONNULL
env_init(env_t * env)
{
    env->root     = NULL;
    env->root_len = 0;
    env->dirfd    = -1;
    env->flags    = 0;
    env->count    = 0;
    env->maxlen   = 0;
    env->most     = NULL;
    env->least    = NULL;
    for (unsigned int i = 0; i < ARRAY_SIZE(env->get); ++i) {
        env->get[i].name    = NULL;
        env->get[i].prefixp = NULL;
    }
    return;
}

void NONNULL
env_deinit(env_t * env)
{
    if (env_ynq(env, ENV_SAVE_BASHRC, ENV_QUERY) ||
        env_ynq(env, ENV_SAVE_CONFIG, ENV_QUERY))
    {
        log_warn("environment changes were not saved");
    }
    while (env->most != NULL) {
        env_prefix_destroy(env, env->most);
    }
    env_close(env);
    free(env->root);
    return;
}

static int NONNULL
env_open_real(env_t * env)
{
    env->dirfd = open(env->root, O_RDWR | O_DIRECTORY | O_PATH);
    if (env->dirfd < 0) {
        log_error_errno("could not open %s", env->root);
        return 1;
    }
    return 0;
}

int NONNULLA(1)
env_create(env_t * env, const char * root)
{
    if (root != NULL) {
        env->root = env_copy_string(root, &env->root_len);
        if (env->root == NULL) {
            goto error;
        }
    } else {
        env->root = env_default_root(&env->root_len);
        if (env->root == NULL) {
            goto error;
        }
    }

    if (mkdir(env->root, S_IRWXU | S_IRGRP | S_IXGRP)) {
        log_error_errno("could not create %s", env->root);
        goto error;
    }

    if (env_open_real(env)) {
        goto error;
    }

    env_ynq(env, ENV_SAVE_BASHRC, ENV_YES);
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    if (env_save(env)) {
        goto error;
    }

    return 0;
error:
    env_close(env);
    return 1;
}

int
env_remove(const char * root)
{
    int    retval  = 1;
    char * dynroot = NULL;

    if (root == NULL) {
        size_t len;
        dynroot = env_default_root(&len);
        if (dynroot == NULL) {
            return 1;
        }
        root = dynroot;
    }

    if (env_remove_recursive(root)) {
        goto error;
    }

    retval = 0;
error:
    free(dynroot);
    return retval;
}

int NONNULLA(1)
env_open(env_t * env, const char * root)
{
    if (root != NULL) {
        env->root = env_copy_string(root, &env->root_len);
        if (env->root == NULL) {
            goto error;
        }
    } else {
        env->root = env_default_root(&env->root_len);
        if (env->root == NULL) {
            goto error;
        }
    }

    if (env_open_real(env)) {
        goto error;
    }

    return 0;
error:
    env_close(env);
    return 1;
}

int NONNULL
env_close(env_t * env)
{
    if (env->dirfd >= 0) {
        if (close(env->dirfd)) {
            log_error_errno("could not close directory");
        }
        env->dirfd = -1;
    }
    if (env->root != NULL) {
        free(env->root);
        env->root = NULL;
    }
    return 0;
}

static env_prefix_t * NONNULL
env_load_prefix(env_t * env, const char * name)
{
    env_prefix_t * prefix;

    prefix = env_prefix_new_least(env, name);
    if (prefix == NULL) {
        return NULL;
    }

    if (env->get[0].name != NULL) {
        int cmp1;

        cmp1 = strcmp(name, env->get[0].name);
        if (cmp1 > 0 && env->get[1].name != NULL) {
            int cmp2;

            cmp2 = strcmp(name, env->get[1].name);
            if (cmp2 == 0) {
                (*env->get[1].prefixp) = prefix;
                env->get[1].name    = NULL;
                env->get[1].prefixp = NULL;
            }
        } else if (cmp1 == 0) {
            (*env->get[0].prefixp) = prefix;
            env->get[0].name    = env->get[1].name;
            env->get[0].prefixp = env->get[1].prefixp;
            env->get[1].name    = NULL;
            env->get[1].prefixp = NULL;
        }
    }

    return prefix;
}

static int NONNULL
env_load_key_and_value(char * str,
                       int count,
                       char ** keyp,
                       char ** valuep)
{
    char * v = str;
    char * e;

    do {
        v++;
    } while (!isspace(*v) && *v != '\0');
    if (*v == '\0') {
        log_error("config.ini:%d: expected value", count);
        return 1;
    }
    *v = '\0';
    do {
        v++;
    } while (isspace(*v));
    if (*v != '=') {
        log_error("config.ini:%d: expected '='", count);
        return 1;
    }
    do {
        v++;
    } while (isspace(*v));
    if (*v == '\0') {
        log_error("config.ini:%d: expected value", count);
        return 1;
    }
    e = v;
    do {
        e++;
    } while (!isspace(*e) && *e != '\0');
    if (*e != '\0') {
        *e = '\0';
        do {
            e++;
        } while (isspace(*e));
        if (*e == '\0') {
            log_error("config.ini:%d: trailing space(s)", count);
            return 1;
        } else {
            log_error("config.ini:%d: junk on line", count);
            return 1;
        }
    }

    *keyp   = str;
    *valuep = v;

    return 0;
}

int NONNULL
env_load(env_t * env)
{
    assert(env->dirfd >= 0);

    int            retval = 1;
    int            fd     = -1;
    FILE *         file   = NULL;
    char *         line   = NULL;
    int            count  = 0;
    env_prefix_t * prefix = NULL;
    size_t         len    = 0;
    ssize_t        ret;

    log_debug("loading config.ini");

    fd = openat(env->dirfd, "config.ini", O_RDONLY);
    if (fd < 0) {
        log_error_errno("could not open config.ini for reading");
        goto error;
    }
    file = fdopen(fd, "r");
    if (file == NULL) {
        log_error_errno("could not open config.ini for reading");
        goto error;
    }

    errno = 0;
    while ((ret = getline(&line, &len, file)) >= 0) {
        char * c = line;
        count++;

        if (ret != 0) {
            if (line[ret-1] == '\n') {
                ret--;
                line[ret] = '\0';
            }

            while (isspace(*c)) {
                ++c;
            }

            if (*c == '[') {
                char * e;

                c++;
                e = strchr(c, ']');
                if (e == NULL) {
                    log_error("config.ini:%d: expected ']'", count);
                    goto error;
                }
                *e = '\0';

                /* before creating prefix make sure row is clean */
                do {
                    e++;
                } while (isspace(*e));
                if (*e != '\0') {
                    log_error("config.ini:%d: junk on line", count);
                    goto error;
                }

                /* check if this prefix is one of the requested by
                 * env_prefix_get */
                prefix = env_load_prefix(env, c);
                if (prefix == NULL) {
                    goto error;
                }
            } else if (*c == '=') {
                log_error("config.ini:%d: syntax error", count);
                goto error;
            } else if (*c != '\0') {
                env_prop_t   key;
                env_prop_t * res;
                env_ynq_t    set;
                char *       k;
                char *       v;

                if (env_load_key_and_value(c, count, &k, &v)) {
                    goto error;
                }

                key.name = k;
                key.flag = 0;
                res = bsearch(&key, env_props, ARRAY_SIZE(env_props),
                              sizeof(env_prop_t), &env_prop_cmp);
                if (res == NULL) {
                    log_error("config.ini:%d: invalid key", count);
                    goto error;
                }

                if (strcmp(v, "no") == 0) {
                    set = ENV_NO;
                } else if (strcmp(v, "yes") == 0) {
                    set = ENV_YES;
                } else {
                    log_error("config.ini:%d: invalid value", count);
                    goto error;
                }

                if (prefix == NULL) {
                    log_error("config.ini:%d: missing prefix", count);
                    goto error;
                }

                env_prefix_ynq(env, prefix, res->flag, set);
            }
        }

        /* errno must not change */
        assert(errno == 0);
    }
    if (errno != 0) {
        log_error_errno("config.ini: could not get line");
        goto error;
    }

    /* Adding the prefixes will set this property to yes, but the config
     * has just been read, so there is no need to rewrite it. */
    env_ynq(env, ENV_SAVE_BASHRC, ENV_NO);
    env_ynq(env, ENV_SAVE_CONFIG, ENV_NO);

    retval = 0;
error:
    free(line);
    if (file != NULL) {
        if (fclose(file)) {
            log_warn_errno("could not close config.ini");
        }
    } else if (fd >= 0) {
        if (close(fd)) {
            log_warn_errno("could not close config.ini");
        }
    }
    return retval;
}

static int NONNULL
env_save_bashrc_iter(env_t * env,
                     env_prefix_t * prefix,
                     void * ptr)
{
    env_save_bashrc_data_t * data = ptr;
    const struct {
        int          flag;
        char **      rootp;
        size_t *     lenp;
        const char * path;
    } list[] = {
        {
            ENV_PREFIX_ACLOCAL,
            &data->aclocal,
            &data->aclocal_len,
            "/share/aclocal"
        },
        {
            ENV_PREFIX_BINS,
            &data->bins,
            &data->bins_len,
            "/bin"
        },
        {
            ENV_PREFIX_SBINS,
            &data->bins,
            &data->bins_len,
            "/sbin"
        },
        {
            ENV_PREFIX_LIBS,
            &data->libs,
            &data->libs_len,
            "/lib"
        },
        {
            ENV_PREFIX_PKGCONFIG,
            &data->pkgconfig,
            &data->pkgconfig_len,
            "/lib/pkgconfig"
        }
    };

    if (!env_prefix_ynq(env, prefix, ENV_PREFIX_EXPORT, ENV_QUERY) ||
        !env_prefix_ynq(env, prefix, ENV_PREFIX_ENABLE, ENV_QUERY))
    {
        return 0;
    }

    for (unsigned int i = 0; i < ARRAY_SIZE(list); ++i) {
        const char * strings[] = {
            "${KE_ROOT}/",
            prefix->name,
            list[i].path,
            ":"
        };
        size_t len = 0;
        char * new;

        if (!env_prefix_ynq(env, prefix, list[i].flag, ENV_QUERY)) {
            continue;
        }

        for (unsigned int j = 0; j < ARRAY_SIZE(strings); ++j) {
            len += strlen(strings[j]);
        }
        new = realloc(*list[i].rootp, *list[i].lenp + len + 1);
        if (new == NULL) {
            log_error_errno("could not allocate memory");
            return 1;
        }
        len = *list[i].lenp;
        for (unsigned int j = 0; j < ARRAY_SIZE(strings); ++j) {
            const size_t tmp = strlen(strings[j]);
            memcpy(new + len, strings[j], tmp);
            len += tmp;
        }
        new[len] = '\0';
        *list[i].rootp = new;
        *list[i].lenp = len;
    }

    return 0;
}

static int NONNULL
env_save_bashrc(env_t * env)
{
    int retval = 1;
    env_save_bashrc_data_t data;
    int fd = -1;
    FILE * file = NULL;

    data.aclocal       = NULL;
    data.aclocal_len   = 0;
    data.bins          = NULL;
    data.bins_len      = 0;
    data.libs          = NULL;
    data.libs_len      = 0;
    data.pkgconfig     = NULL;
    data.pkgconfig_len = 0;

    log_debug("saving bashrc");

    if (env_iter_forward(env, &env_save_bashrc_iter, &data)) {
        goto error;
    }

    fd = openat(env->dirfd, "bashrc", O_WRONLY | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR | S_IRGRP);
    if (fd < 0) {
        log_error_errno("could not open bashrc for writing");
        goto error;
    }

    file = fdopen(fd, "w");
    if (file == NULL) {
        log_error_errno("could not open bashrc for writing");
        goto error;
    }

    fprintf(file, "export KE_ROOT=\"%s\"\n\n", env->root);

    const struct {
        const char * var;
        char *       str;
        size_t       len;
    } vars[] = {
        {"ACLOCAL_PATH",    data.aclocal,   data.aclocal_len},
        {"PATH",            data.bins,      data.bins_len},
        {"LD_LIBRARY_PATH", data.libs,      data.libs_len},
        {"PKG_CONFIG_PATH", data.pkgconfig, data.pkgconfig_len}
    };

    for (unsigned int i = 0; i < ARRAY_SIZE(vars); ++i) {
        if (vars[i].str != NULL) {
            /* remove trailing ':' */
            vars[i].str[vars[i].len-1] = '\0';

            fprintf(file,
                    "if test -n \"${%s}\"\n"
                    "then\n"
                    "    export %s=\"%s:${%s}\"\n"
                    "else\n"
                    "    export %s=\"%s\"\n"
                    "fi\n\n",
                    vars[i].var, vars[i].var, vars[i].str, vars[i].var,
                    vars[i].var, vars[i].str);
        }
    }

    log_info("you must reload bashrc for the changes to take effect");

    retval = 0;
error:
    if (file != NULL) {
        if (fclose(file)) {
            log_warn_errno("could not close bashrc");
        }
    } else if (fd >= 0) {
        if (close(fd)) {
            log_warn_errno("could not close bashrc file descriptor");
        }
    }
    free(data.aclocal);
    free(data.bins);
    free(data.libs);
    free(data.pkgconfig);
    return retval;
}

static int NONNULL
env_save_config_iter(env_t * env,
                     env_prefix_t * prefix,
                     void * data)
{
    FILE * file = data;
    int    is_on;

    fprintf(file, "[%s]\n", prefix->name);
    for (unsigned int i = 0; i < ARRAY_SIZE(env_props); ++i) {
        is_on = env_prefix_ynq(env, prefix, env_props[i].flag,
                               ENV_QUERY);
        fprintf(file, "    %-9s = %s\n", env_props[i].name,
                is_on ? "yes" : "no");
    }
    fputc('\n', file);
    return 0;
}

static int NONNULL
env_save_config(env_t * env)
{
    int    retval = 1;
    int    fd     = -1;
    FILE * file   = NULL;

    log_debug("saving config.ini");

    fd = openat(env->dirfd, "config.ini", O_WRONLY | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR | S_IRGRP);
    if (fd < 0) {
        log_error_errno("could not open config.ini for writing");
        goto error;
    }

    file = fdopen(fd, "w");
    if (file == NULL) {
        log_error_errno("could not open config.ini for writing");
        goto error;
    }

    if (env_iter_forward(env, &env_save_config_iter, file)) {
        goto error;
    }

    retval = 0;
error:
    if (file != NULL) {
        if (fclose(file)) {
            log_warn_errno("could not close config.ini");
        }
    } else if (fd >= 0) {
        if (close(fd)) {
            log_warn_errno("could not close config.ini");
        }
    }
    return retval;
}

int NONNULL
env_save(env_t * env)
{
    if (env_ynq(env, ENV_SAVE_BASHRC, ENV_QUERY)) {
        if (env_save_bashrc(env)) {
            return 1;
        }
        env_ynq(env, ENV_SAVE_BASHRC, ENV_NO);
    }
    if (env_ynq(env, ENV_SAVE_CONFIG, ENV_QUERY)) {
        if (env_save_config(env)) {
            return 1;
        }
        env_ynq(env, ENV_SAVE_CONFIG, ENV_NO);
    }
    return 0;
}

int NONNULL
env_ynq(env_t * env, int flag, env_ynq_t ynq)
{
    return env_flag_ynq(&env->flags, flag, ynq);
}

int NONNULLA(1,2)
env_iter_forward(env_t * env, env_iter_func_t * func, void * data)
{
    env_prefix_t * this;
    env_prefix_t * next = env->most;
    int ret;
    while (next != NULL) {
        this = next;
        next = this->less;
        ret = (*func)(env, this, data);
        if (ret) {
            return ret;
        }
    }
    return 0;
}

int NONNULLA(1,2)
env_iter_backward(env_t * env, env_iter_func_t * func, void * data)
{
    env_prefix_t * this;
    env_prefix_t * next = env->least;
    int ret;
    while (next != NULL) {
        this = next;
        next = this->more;
        ret = (*func)(env, this, data);
        if (ret) {
            return ret;
        }
    }
    return 0;
}

/* must be called before env_load() */
void NONNULL
env_prefix_get(env_t * env, const char * name, env_prefix_t ** prefixp)
{
    assert(env->most  == NULL);
    assert(env->least == NULL);
    if (env->get[0].name == NULL) {
        env->get[0].name    = name;
        env->get[0].prefixp = prefixp;
    } else if (env->get[1].name == NULL) {
        env->get[1].name    = name;
        env->get[1].prefixp = prefixp;
    } else {
        log_fatal("%s may only be called at most two times",
                  __func__);
        abort();
    }
    return;
}

static env_prefix_t * NONNULL
env_prefix_new(env_t * env, const char * name)
{
    env_prefix_t * prefix;
    size_t         len;

    len = strlen(name);
    prefix = malloc(sizeof(env_prefix_t) + len + 1);
    if (prefix == NULL) {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    /* enable all flags except export by default */
    prefix->flags = ~ENV_PREFIX_EXPORT;
    prefix->more = NULL;
    prefix->less = NULL;
    prefix->len = len;
    memcpy(prefix->name, name, len + 1);

    env->count++;
    if (len > env->maxlen) {
        env->maxlen = len;
    }
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);

    return prefix;
}

static inline void NONNULLA(1,2)
env_prefix_link(env_t * env,
                env_prefix_t * prefix,
                env_prefix_t * more,
                env_prefix_t * less)
{
    prefix->more = more;
    if (more != NULL) {
        more->less = prefix;
    } else {
        env->most = prefix;
    }

    prefix->less = less;
    if (less != NULL) {
        less->more = prefix;
    } else {
        env->least = prefix;
    }
    return;
}

env_prefix_t * NONNULL
env_prefix_new_most(env_t * env, const char * name)
{
    env_prefix_t * prefix = env_prefix_new(env, name);
    if (prefix != NULL) {
        env_prefix_link(env, prefix, NULL, env->most);
    }
    return prefix;
}

env_prefix_t * NONNULL
env_prefix_new_least(env_t * env, const char * name)
{
    env_prefix_t * prefix = env_prefix_new(env, name);
    if (prefix != NULL) {
        env_prefix_link(env, prefix, env->least, NULL);
    }
    return prefix;
}

static void NONNULL
env_prefix_unlink(env_t * env, env_prefix_t * prefix)
{
    env_prefix_t * more = prefix->more;
    env_prefix_t * less = prefix->less;
    if (more != NULL) {
        more->less = less;
    } else {
        assert(prefix == env->most);
        env->most = less;
    }
    if (less != NULL) {
        less->more = more;
    } else {
        assert(prefix == env->least);
        env->least = more;
    }
    return;
}

void NONNULL
env_prefix_destroy(env_t * env, env_prefix_t * prefix)
{
    /* update env properties */
    env->count--;
    if (env_prefix_ynq(env, prefix, ENV_PREFIX_EXPORT, ENV_QUERY)) {
        env_ynq(env, ENV_SAVE_BASHRC, ENV_YES);
    }
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    env_prefix_unlink(env, prefix);
    free(prefix);
    return;
}

int NONNULL
env_prefix_create(env_t * env, env_prefix_t * prefix)
{
    assert(env->dirfd >= 0);

    if (mkdirat(env->dirfd, prefix->name, S_IRWXU | S_IRGRP)) {
        log_error_errno("could not create directory %s", prefix->name);
        return 1;
    }

    return 0;
}

int NONNULL
env_prefix_remove(env_t * env, env_prefix_t * prefix)
{
    int    retval = 1;
    char * path   = NULL;
    size_t root_len;
    size_t name_len;

    root_len = strlen(env->root);
    name_len = prefix->len;
    path = malloc(root_len + name_len + 2);
    if (path == NULL) {
        log_error_errno("could not allocate memory");
        goto error;
    }
    memcpy(path, env->root, root_len);
    path[root_len] = '/';
    memcpy(path + root_len + 1, prefix->name, name_len + 1);

    if (env_remove_recursive(path)) {
        goto error;
    }

    retval = 0;
error:
    free(path);
    return retval;
}

void NONNULL
env_prefix_move_first(env_t * env,
                      env_prefix_t * prefix)
{
    env_prefix_unlink(env, prefix);
    env_prefix_link(env, prefix, NULL, env->most);
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    return;
}

void NONNULL
env_prefix_move_last(env_t * env,
                     env_prefix_t * prefix)
{
    env_prefix_unlink(env, prefix);
    env_prefix_link(env, prefix, env->least, NULL);
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    return;
}

void NONNULL
env_prefix_move_before(env_t * env,
                       env_prefix_t * prefix,
                       env_prefix_t * less)
{
    assert(prefix != less);
    env_prefix_unlink(env, prefix);
    env_prefix_link(env, prefix, less->more, less);
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    return;
}

void NONNULL
env_prefix_move_after(env_t * env,
                      env_prefix_t * prefix,
                      env_prefix_t * more)
{
    assert(prefix != more);
    env_prefix_unlink(env, prefix);
    env_prefix_link(env, prefix, more, more->less);
    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    return;
}

void NONNULL
env_prefix_swap(env_t * env,
                env_prefix_t * prefix1,
                env_prefix_t * prefix2)
{
    env_prefix_t * less;
    env_prefix_t * more;

    assert(prefix1 != prefix2);

    less = prefix1->less;
    more = prefix1->more;
    env_prefix_link(env, prefix1, prefix2->more, prefix2->less);
    env_prefix_link(env, prefix2, more, less);

    env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
    return;
}

int NONNULL
env_prefix_ynq(env_t * env,
               env_prefix_t * prefix,
               int flag,
               env_ynq_t ynq)
{
    if (ynq != ENV_QUERY) {
        int is_on = env_prefix_ynq(env, prefix, flag, ENV_QUERY);
        if ((is_on && ynq == ENV_NO) || (!is_on && ynq == ENV_YES)) {
            env_ynq(env, ENV_SAVE_CONFIG, ENV_YES);
            if (env_prefix_ynq(env, prefix, ENV_PREFIX_EXPORT,
                               ENV_QUERY) ||
                flag == ENV_PREFIX_EXPORT)
            {
                env_ynq(env, ENV_SAVE_BASHRC, ENV_YES);
            }
        }
    }
    return env_flag_ynq(&prefix->flags, flag, ynq);
}

static inline unsigned int CONST
digits(unsigned int num)
{
    unsigned int digits = 0;
    for (unsigned int pow = 1; pow <= num; pow *= 10) {
        digits++;
    }
    return digits;
}

void NONNULL
env_prefix_print(env_t * env,
                 env_prefix_t * prefix,
                 unsigned int num,
                 FILE * file)
{
    if (num != 0) {
        unsigned int num_digits = digits(num);
        unsigned int cnt_digits = digits(env->count);
        fputc('(', file);
        while (num_digits < cnt_digits) {
            num_digits++;
            fputc('0', file);
        }
        fprintf(file, "%u) ", num);
    }

    fputs(prefix->name, file);
    for (size_t i = prefix->len; i < env->maxlen; ++i) {
        fputc(' ', file);
    }
    fputs(" [", file);

    const struct {
        int flag;
        char on;
        char off;
    } flags[] = {
        {ENV_PREFIX_ENABLE,    'e', 'd'},
        {ENV_PREFIX_EXPORT,    'x', 'u'},
        {ENV_PREFIX_INSTALL,   'i', ' '},
        {ENV_PREFIX_ACLOCAL,   'a', ' '},
        {ENV_PREFIX_BINS,      'b', ' '},
        {ENV_PREFIX_SBINS,     's', ' '},
        {ENV_PREFIX_LIBS,      'l', ' '},
        {ENV_PREFIX_PKGCONFIG, 'p', ' '},
    };
    for (unsigned int i = 0; i < ARRAY_SIZE(flags); ++i) {
        if (env_prefix_ynq(env, prefix, flags[i].flag, ENV_QUERY)) {
            fputc(flags[i].on, file);
        } else {
            fputc(flags[i].off, file);
        }
    }

    fputs("]\n", file);
    return;
}

static void NONNULL
env_execvar_incr(env_execvar_t * var, size_t len)
{
    var->tlen += len;
    var->nstr++;
    return;
}

static void NONNULL
env_execvar_decr(env_execvar_t * var, size_t len)
{
    var->tlen -= len;
    var->nstr--;
    return;
}

static env_execvar_str_t * NONNULL
env_execvar_str_alloc(env_execvar_t * var, size_t len)
{
    env_execvar_str_t * s = malloc(sizeof(env_execvar_str_t) + len + 1);
    if (s == NULL) {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    s->prev = NULL;
    s->next = NULL;
    s->len  = len;

    env_execvar_incr(var, len);

    return s;
}

static env_execvar_str_t * NONNULL
env_execvar_str_new(env_execvar_t * var,
                    const char * str,
                    size_t len)
{
    env_execvar_str_t * s;
    s = env_execvar_str_alloc(var, len);
    if (s != NULL) {
        memcpy(s->str, str, len);
        s->str[len] = '\0';
    }
    return s;
}

static void NONNULL
env_execvar_str_destroy(env_execvar_t * var, env_execvar_str_t * str)
{
    env_execvar_str_t * prev = str->prev;
    env_execvar_str_t * next = str->next;
    if (prev != NULL) {
        prev->next = next;
    } else {
        if (str == var->head) {
            var->head = next;
        } else {
            assert(str == var->tail);
            var->tail = next;
        }
    }
    if (next != NULL) {
        next->prev = prev;
    }

    env_execvar_decr(var, str->len);
    free(str);
    return;
}

static int NONNULL
env_execvar_str_cmp(const void * p1, const void * p2)
{
    const env_execvar_str_t * const * s1p = p1;
    const env_execvar_str_t * const * s2p = p2;
    return strcmp((*s1p)->str, (*s2p)->str);
}

static void NONNULL
env_execvar_init(env_execvar_t * var)
{
    var->head         = NULL;
    var->tail         = NULL;
    var->tail_arr_siz = 0;
    var->tail_arr     = NULL;
    var->nstr         = 0;
    var->tlen         = 0;
    var->result       = NULL;
    return;
}

static void NONNULL
env_execvar_deinit(env_execvar_t * var)
{
    while (var->head != NULL) {
        env_execvar_str_destroy(var, var->head);
    }
    while (var->tail != NULL) {
        env_execvar_str_destroy(var, var->tail);
    }
    free(var->tail_arr);
    free(var->result);
    return;
}

static void NONNULL
env_execvar_unget(env_execvar_t * var)
{
    free(var->result);
    var->result = NULL;
    return;
}

static int NONNULL
env_execvar_readenv(env_execvar_t * var, const char * envvar)
{
    env_execvar_str_t * last = NULL;
    const char *        value;
    const char *        colon;
    size_t              cnt = 0;

    assert(var->tail_arr_siz == 0);

    env_execvar_unget(var);

    value = getenv(envvar);
    if (value == NULL || *value == '\0') {
        /* great! nothing to do */
        return 0;
    }

    colon = value - 1;
    while (colon != NULL) {
        size_t              len;
        env_execvar_str_t * str;

        value = colon + 1;
        colon = strchr(value, ':');
        if (colon != NULL) {
            len = (size_t)(colon - value);
        } else {
            len = strlen(value);
        }

        str = env_execvar_str_new(var, value, len);
        if (str == NULL) {
            return 1;
        }

        /* append to the tail */
        str->prev = last;
        if (last != NULL) {
            assert(var->tail != NULL);
            last->next = str;
        } else {
            assert(var->tail == NULL);
            var->tail = str;
        }
        last = str;

        var->tail_arr_siz++;
    }

    var->tail_arr = malloc(var->tail_arr_siz *
                           sizeof(env_execvar_str_t *));
    if (var->tail_arr == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }
    for (env_execvar_str_t * str = var->tail;
         str != NULL;
         str = str->next)
    {
        var->tail_arr[cnt++] = str;
    }

    qsort(var->tail_arr, var->tail_arr_siz, sizeof(env_execvar_str_t *),
          &env_execvar_str_cmp);

    return 0;
}

/* Adds a path to the execvar at the beginning of the head (highest
 * priority), removing the same path from. */
static int NONNULL
env_execvar_add(env_execvar_t * var,
                const env_t * env,
                const env_prefix_t * prefix,
                const char * path)
{
    size_t               path_len;
    size_t               len;
    env_execvar_str_t *  str;
    env_execvar_str_t *  next;
    env_execvar_str_t ** foundp;

    log_debug("%s (%s,%s)", __func__, prefix->name, path);

    env_execvar_unget(var);

    path_len = strlen(path);
    len = env->root_len + prefix->len + path_len + 1;
    str = env_execvar_str_alloc(var, len);
    if (str == NULL) {
        return 1;
    }

    memcpy(str->str, env->root, env->root_len);
    str->str[env->root_len] = '/';
    memcpy(str->str + env->root_len + 1, prefix->name, prefix->len);
    memcpy(str->str + env->root_len + 1 + prefix->len, path, path_len);
    str->str[len] = '\0';

    next = var->head;
    str->next = next;
    if (next != NULL) {
        next->prev = str;
    }
    var->head = str;

duplicate:
    foundp = bsearch(&str, var->tail_arr, var->tail_arr_siz,
                     sizeof(env_execvar_str_t *), &env_execvar_str_cmp);
    if (foundp != NULL) {
        env_execvar_str_t * found;
        size_t i;
        size_t rest;

        /* First of all save the found str at foundp, since it will be
         * overwritten. */
        found = *foundp;

        log_debug("found %s in tail, removing since it is in head",
                  str->str);

        /* The index i is to be removed from the array, so move the rest
         * of the array, starting at i+1, to i. */
        var->tail_arr_siz--;
        i = (size_t)(foundp - var->tail_arr);
        assert(i < var->tail_arr_siz);
        assert(var->tail_arr[i] == found);

        rest = var->tail_arr_siz - i;
        memmove(var->tail_arr + i, var->tail_arr + i + 1,
                rest * sizeof(env_execvar_str_t *));

        /* Destroy the found execvar_str_t */
        env_execvar_str_destroy(var, found);
        goto duplicate;
    }

    return 0;
}

static void NONNULLA(2,3)
env_execvar_get_iter(env_execvar_str_t * str,
                     char * result,
                     size_t * posp)
{
    size_t pos = *posp;
    for (; str != NULL; str = str->next) {
        memcpy(result + pos, str->str, str->len);
        pos += str->len;
        result[pos] = ':';
        pos++;
    }
    *posp = pos;
    return;
}

static const char * NONNULL
env_execvar_get(env_execvar_t * var)
{
    size_t pos = 0;

    if (var->nstr == 0) {
        assert(var->tlen == 0);
        return "";
    } else if (var->result == NULL) {
        assert(var->head != NULL || var->tail != NULL);

        var->result = malloc(var->tlen + var->nstr);
        if (var->result == NULL) {
            log_error_errno("could not allocate memory");
            return NULL;
        }

        env_execvar_get_iter(var->head, var->result, &pos);
        env_execvar_get_iter(var->tail, var->result, &pos);
        assert(pos > 0);
        var->result[pos-1] = '\0';
    }

    return var->result;
}

static int NONNULL
env_exec_iter(env_t * env, env_prefix_t * prefix, void * ptr)
{
    env_exec_data_t * data = ptr;

    const struct {
        int flag;
        env_execvar_t * var;
        const char *    path;
    } vars[] = {
        {ENV_PREFIX_ACLOCAL,   &data->aclocal,   "/share/aclocal"},
        {ENV_PREFIX_BINS,      &data->bins,      "/bin"},
        {ENV_PREFIX_SBINS,     &data->bins,      "/sbin"},
        {ENV_PREFIX_LIBS,      &data->libs,      "/lib"},
        {ENV_PREFIX_PKGCONFIG, &data->pkgconfig, "/lib/pkgconfig"}
    };

    log_debug("%s (%s) begin", __func__, prefix->name);

    if (env_prefix_ynq(env, prefix, ENV_PREFIX_ENABLE, ENV_QUERY)) {
        log_debug("%s (%s) enabled", __func__, prefix->name);
        for (size_t i = 0; i < ARRAY_SIZE(vars); ++i) {
            if (env_prefix_ynq(env, prefix, vars[i].flag, ENV_QUERY) &&
                env_execvar_add(vars[i].var, env, prefix, vars[i].path))
            {
                return 1;
            }
        }
    } else {
        log_debug("%s (%s) disabled", __func__, prefix->name);
    }

    log_debug("%s (%s) end", __func__, prefix->name);

    return 0;
}

int NONNULL
env_exec(env_t * env, char * const * argv)
{
    int err = 0;
    env_exec_data_t data;
    size_t ni; /* [n]umber of [i]nitialized execvars */
    const struct {
        const char *    envvar;
        env_execvar_t * execvar;
    } vars[] = {
        {"ACLOCAL_PATH",    &data.aclocal},
        {"PATH",            &data.bins},
        {"LD_LIBRARY_PATH", &data.libs},
        {"PKG_CONFIG_PATH", &data.pkgconfig}
    };

    for (ni = 0; ni < ARRAY_SIZE(vars); ++ni) {
        env_execvar_init(vars[ni].execvar);
        if (env_execvar_readenv(vars[ni].execvar, vars[ni].envvar)) {
            goto error;
        }
    }

    if (env_iter_backward(env, &env_exec_iter, &data)) {
        goto error;
    }

    const char * value;
    for (size_t i = 0; i < ARRAY_SIZE(vars); ++i) {
        value = env_execvar_get(vars[i].execvar);
        if (value == NULL) {
            goto error;
        }
        if (*value != '\0') {
            if (setenv(vars[i].envvar, value, 1)) {
                goto error;
            }
        } else  if (unsetenv(vars[i].envvar)) {
            goto error;
        }
    }

    if (0) {
    error:
        err = 1;
    }

    for (size_t i = 0; i < ni; ++i) {
        env_execvar_deinit(vars[i].execvar);
    }

    if (!err) {
        int ret UNUSED;
        ret = execvp(argv[0], argv);
        assert(ret < 0);
        log_error_errno("could not exec");
        /* err = 1; */
    }

    return -1;
}

int NONNULL
env_run(env_t * env, char * const * argv)
{
    pid_t pid;
    int   status;

    pid = fork();
    if (pid == 0) {
        /* child process */
        env_exec(env, argv);
        exit(1); /* if this is reached, exec failed */
    } else if (pid < 0) {
        log_error_errno("could not fork");
        return -1;
    }

    assert(pid > 0);
    do {
        pid_t ret;
        ret = waitpid(pid, &status, 0);
        if (ret < 0) {
            log_error_errno("waitpid failed");
            return -1;
        }
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));

    return WEXITSTATUS(status);
}

const char * NONNULL RETURNS_NONNULL
env_root(env_t * env, size_t * lenp)
{
    *lenp = env->root_len;
    return env->root;
}

const char * NONNULL RETURNS_NONNULL
env_prefix_name(env_prefix_t * prefix, size_t * lenp)
{
    *lenp = prefix->len;
    return prefix->name;
}
