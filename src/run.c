/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "common.h"
#include "env.h"
#include "log.h"

static int args_given = 0;

static gop_return_t NONNULL
main_args(gop_t * const gop)
{
    args_given = 1;
    return GOP_DO_RETURN;
}

static gop_return_t NONNULL
main_error(gop_t * const gop)
{
    gop_error_t err;

    err = gop_get_error(gop);
    switch (err) {
        case GOP_ERROR_UNKLOPT:
        case GOP_ERROR_UNKSOPT:
            return GOP_DO_CONTINUE;
        default:
            return GOP_DO_ERROR;
    }
}

int
main(int argc, char ** argv)
{
    int exit_status = EXIT_FAILURE;

    int          fork = 0;
    const char * root = NULL;

    env_t env;
    env_init(&env);

    const gop_option_t options[] = {
        {"args", '-', GOP_NONE, NULL, &main_args,
            "arguments after this option is treated as command and "
            "arguments", NULL},
        {"fork", 'f', GOP_NONE, &fork, NULL, "fork before exec",
            NULL},
        {"root", 'R', GOP_STRING, &root, NULL,
            "use environment root ROOT", "ROOT"},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    log_set_level(LOG_LEVEL_WARN);

    gop_add_usage(gop, "[OPTION...] [--|--args] command "
                       "[arguments...]");
    gop_description(gop, "Run command with arguments in the "
                         "%s environment.", PACKAGE);
    gop_add_table(gop, "Program options:", options);
    gop_aterror(gop, &main_error);
    common_gop(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (args_given) {
        argv++;
    }

    if (argc < 2) {
        log_error("invalid number of arguments");
        return EXIT_FAILURE;
    }

    if (env_open(&env, root)) {
        goto error;
    }
    if (env_load(&env)) {
        goto error;
    }

    if (fork) {
        if (env_run(&env, argv + 1)) {
            goto error;
        }
    } else if (env_exec(&env, argv + 1)) {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    env_close(&env);
    env_deinit(&env);
    return exit_status;
}
