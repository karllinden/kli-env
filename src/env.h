/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ENV_H
# define ENV_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include "common.h"

# define ENV_SAVE_BASHRC 0x1
# define ENV_SAVE_CONFIG 0x2

# define ENV_PREFIX_ENABLE    0x01
# define ENV_PREFIX_EXPORT    0x02
# define ENV_PREFIX_INSTALL   0x04
# define ENV_PREFIX_ACLOCAL   0x08
# define ENV_PREFIX_BINS      0x10
# define ENV_PREFIX_SBINS     0x20
# define ENV_PREFIX_LIBS      0x40
# define ENV_PREFIX_PKGCONFIG 0x80

enum env_ynq_e {
    ENV_YES,
    ENV_NO,
    ENV_QUERY
};
typedef enum env_ynq_e env_ynq_t;

struct env_prefix_s {
    int flags;

    /* links in the list to the more preferred (prev) and to the least
     * preferred (next) */
    struct env_prefix_s * more;
    struct env_prefix_s * less;

    size_t len;
    char name[];
};
typedef struct env_prefix_s env_prefix_t;

struct env_get_s {
    const char *    name;
    env_prefix_t ** prefixp;
};
typedef struct env_get_s env_get_t;

struct env_s {
    char * root;
    size_t root_len;

    int dirfd;
    int flags;

    /* count of the number of prefixes and maximum length of their
     * names */
    unsigned int count;
    size_t       maxlen;

    /* links to the most preferred (first) and the least (last)
     * preferred prefix */
    env_prefix_t * most;
    env_prefix_t * least;

    /* ordered so that strcmp(get[0].name, get[1].name) < 0 */
    env_get_t get[2];
};
typedef struct env_s env_t;

typedef int (env_iter_func_t)(env_t * env,
                              env_prefix_t * prefix,
                              void * data);

void           env_init              (env_t * env) NONNULL;
void           env_deinit            (env_t * env) NONNULL;
int            env_create            (env_t * env,
                                      const char * root) NONNULLA(1);
int            env_remove            (const char * root);
int            env_open              (env_t * env,
                                      const char * root) NONNULLA(1);
int            env_close             (env_t * env) NONNULL;

int            env_load              (env_t * env) NONNULL;
int            env_save              (env_t * env) NONNULL;

int            env_ynq               (env_t * env,
                                      int flag,
                                      env_ynq_t ynq) NONNULL;

/* Iterate over the prefixes in the env_t. Forward is from most
 * preferred to least preferred and backward is the opposite. */
int            env_iter_forward      (env_t * env,
                                      env_iter_func_t * func,
                                      void * data) NONNULLA(1,2);
int            env_iter_backward     (env_t * env,
                                      env_iter_func_t * func,
                                      void * data) NONNULLA(1,2);

/* must be called before env_load() */
void           env_prefix_get        (env_t * env,
                                      const char * name,
                                      env_prefix_t ** prefixp) NONNULL;

env_prefix_t * env_prefix_new_least  (env_t * env,
                                      const char * name) NONNULL;
env_prefix_t * env_prefix_new_most   (env_t * env,
                                      const char * name) NONNULL;
void           env_prefix_destroy    (env_t * env,
                                      env_prefix_t * prefix) NONNULL;

int            env_prefix_create     (env_t * env,
                                      env_prefix_t * prefix) NONNULL;
int            env_prefix_remove     (env_t * env,
                                      env_prefix_t * prefix) NONNULL;

void           env_prefix_move_first (env_t * env,
                                      env_prefix_t * prefix) NONNULL;
void           env_prefix_move_last  (env_t * env,
                                      env_prefix_t * prefix) NONNULL;
void           env_prefix_move_before(env_t * env,
                                      env_prefix_t * prefix,
                                      env_prefix_t * less) NONNULL;
void           env_prefix_move_after (env_t * env,
                                      env_prefix_t * prefix,
                                      env_prefix_t * more) NONNULL;

void           env_prefix_swap       (env_t * env,
                                      env_prefix_t * prefix1,
                                      env_prefix_t * prefix2) NONNULL;

int            env_prefix_ynq        (env_t * env,
                                      env_prefix_t * prefix,
                                      int flag,
                                      env_ynq_t ynq) NONNULL;

void           env_prefix_print      (env_t * env,
                                      env_prefix_t * prefix,
                                      unsigned int num,
                                      FILE * file) NONNULL;

/* returns a negative value on error, otherwise does not return */
int            env_exec              (env_t * env,
                                      char * const * argv) NONNULL;

/* returns a negative value on error or the exit status from the command
 * on success */
int            env_run               (env_t * env,
                                      char * const * argv) NONNULL;

const char *   env_root              (env_t * env,
                                      size_t * lenp) NONNULL RETURNS_NONNULL;
const char *   env_prefix_name       (env_prefix_t * prefix,
                                      size_t * lenp) NONNULL RETURNS_NONNULL;

#endif /* !ENV_H */
