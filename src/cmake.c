/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "common.h"
#include "env.h"
#include "log.h"

static gop_return_t NONNULL
main_args(gop_t * const gop)
{
    return GOP_DO_RETURN;
}

static gop_return_t NONNULL
main_error(gop_t * const gop)
{
    gop_error_t err;

    err = gop_get_error(gop);
    switch (err) {
        case GOP_ERROR_UNKLOPT:
        case GOP_ERROR_UNKSOPT:
            return GOP_DO_CONTINUE;
        default:
            return GOP_DO_ERROR;
    }
}

static int
main_iter(env_t * env, env_prefix_t * prefix, void * data)
{
    env_prefix_t ** prefixp = data;
    if (env_prefix_ynq(env, prefix, ENV_PREFIX_INSTALL, ENV_QUERY)) {
        *prefixp = prefix;
        return 1;
    }
    return 0;
}

int
main(int argc, char ** argv)
{
    int exit_status = EXIT_FAILURE;

    int          fork   = 0;
    const char * prefix = NULL;
    const char * root   = NULL;

    env_t env;
    env_init(&env);

    env_prefix_t * install = NULL;

    size_t        argcs;
    char **       cmdline   = NULL;
    char *        prefixarg = NULL;
    const char *  argprefix = "-DCMAKE_INSTALL_PREFIX=";
    size_t        argprefixlen = strlen(argprefix);
    const char *  r;
    size_t        rlen;
    const char *  n;
    size_t        nlen;

    const gop_option_t options[] = {
        {"args", '-', GOP_NONE, NULL, &main_args,
            "arguments after this option is treated as arguments",
            NULL},
        {"prefix", 'p', GOP_STRING, &prefix, NULL,
            "install into PREFIX instead of first installable prefix",
            "PREFIX"},
        {"fork", 'f', GOP_NONE, &fork, NULL, "fork before exec",
            NULL},
        {"root", 'R', GOP_STRING, &root, NULL,
            "use environment root ROOT", "ROOT"},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    log_set_level(LOG_LEVEL_WARN);

    gop_add_usage(gop, "[OPTION...] [--|--args] [arguments...]");
    gop_description(gop, "Runs cmake in the %s environment with "
                         "specified arguments and the --prefix option "
                         "set to the first installable prefix.\n",
                         PACKAGE);
    gop_add_table(gop, "Program options:", options);
    gop_aterror(gop, &main_error);
    common_gop(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    for (int i = 0; i < argc; ++i) {
        log_debug("argv[%d] = %s", i, argv[i]);
    }

    if (prefix != NULL) {
        env_prefix_get(&env, prefix, &install);
    }

    if (env_open(&env, root)) {
        goto error;
    }
    if (env_load(&env)) {
        goto error;
    }

    if (prefix != NULL) {
        if (install == NULL) {
            log_error("could not find prefix %s", prefix);
            goto error;
        }
    } else {
        env_iter_forward(&env, &main_iter, &install);
        if (install == NULL) {
            log_error("no installable prefix found");
            goto error;
        }
    }

    argcs = (size_t)argc;
    cmdline = malloc((argcs + 2) * sizeof(const char *));
    if (cmdline == NULL) {
        log_error_errno("could not allocate memory");
        goto error;
    }
    cmdline[0] = getenv("CMAKE");
    if (cmdline[0] == NULL) {
        cmdline[0] = "cmake";
    }

    r = env_root(&env, &rlen);
    n = env_prefix_name(install, &nlen);
    prefixarg = malloc(argprefixlen + rlen + 1 + nlen + 1);
    if (prefixarg == NULL) {
        log_error_errno("could not allocate memory");
        goto error;
    }
    memcpy(prefixarg, argprefix, argprefixlen);
    memcpy(prefixarg + argprefixlen, r, rlen);
    prefixarg[argprefixlen + rlen] = '/';
    memcpy(prefixarg + argprefixlen + rlen + 1, n, nlen + 1);
    cmdline[1] = prefixarg;
    memcpy(cmdline + 2, argv + 1, argcs * sizeof(char *));

    for (size_t i = 0; i < argcs + 1; ++i) {
        log_debug("cmdline[%ld] = %s", i, cmdline[i]);
    }

    if (fork) {
        if (env_run(&env, cmdline)) {
            goto error;
        }
    } else if (env_exec(&env, cmdline)) {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    free(prefixarg);
    free(cmdline);
    env_close(&env);
    env_deinit(&env);
    return exit_status;
}
