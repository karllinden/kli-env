/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "common.h"
#include "log.h"

static gop_callback_t common_version_cb;
static gop_callback_t common_quiet_cb;
static gop_callback_t common_loglevel_cb;

static char *         common_loglevel = NULL;

static gop_option_t common_table[] = {
    {"loglevel", 'l', GOP_STRING, &common_loglevel, &common_loglevel_cb,
        "fatal|error|warn|info|debug"},
    {"quiet", 'q', GOP_NONE, NULL, &common_quiet_cb,
        "do not print log messages", NULL},
    {"version", 'v', GOP_NONE, NULL, &common_version_cb,
        "print version and exit", NULL},
    GOP_TABLEEND
};

static gop_return_t NONNULL
common_loglevel_cb(gop_t * const gop)
{
    log_level_t level = log_level_from_string(common_loglevel);
    if (level == LOG_LEVEL_NONE) {
        log_error("%s is not a valid loglevel", common_loglevel);
        gop_set_exit_status(gop, EXIT_FAILURE);
        return GOP_DO_EXIT;
    }
    log_set_level(level);
    return GOP_DO_CONTINUE;
}

static gop_return_t NONNULL
common_quiet_cb(gop_t * const gop)
{
    log_set_quiet(1);
    return GOP_DO_CONTINUE;
}

static gop_return_t NONNULL
common_version_cb(gop_t * const gop)
{
    const char * const license = "\
License GPLv3+: GNU GPL version 3 or later\n\
<http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n";

    printf("%s (%s) %s\n"
           "Copyright (C) 2014-2015 Karl Linden\n"
           "%s", PROGRAM_NAME, PACKAGE, VERSION, license);
    return GOP_DO_EXIT;
}

int NONNULL
common_gop(gop_t * const gop)
{
    gop_extra_help(gop,
                   "Report bugs to: %s\n"
                   "%s home page: %s",
                   BUGREPORT, PACKAGE, HOMEPAGE);
    gop_add_table(gop, "Common options:", common_table);
    return 0;
}
