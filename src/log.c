/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2014-2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "log.h"

static log_level_t log_level = LOG_LEVEL_DEBUG;
static int         log_quiet = 0;

void
log_set_level(const log_level_t level)
{
    assert(level != LOG_LEVEL_NONE);
    log_level = level;
    return;
}

void
log_set_quiet(const int quiet)
{
    log_quiet = quiet;
    return;
}

log_level_t NONNULL PURE
log_level_from_string(const char * const string)
{
    if (strcmp(string, "fatal") == 0) {
        return LOG_LEVEL_FATAL;
    } else if (strcmp(string, "error") == 0) {
        return LOG_LEVEL_ERROR;
    } else if (strcmp(string, "warn") == 0) {
        return LOG_LEVEL_WARN;
    } else if (strcmp(string, "info") == 0) {
        return LOG_LEVEL_INFO;
    } else if (strcmp(string, "debug") == 0) {
        return LOG_LEVEL_DEBUG;
    } else {
        return LOG_LEVEL_NONE;
    }
}

static const char * CONST RETURNS_NONNULL
log_level_to_string(const log_level_t level)
{
    switch (level) {
        case LOG_LEVEL_FATAL:
            return "fatal";
        case LOG_LEVEL_ERROR:
            return "error";
        case LOG_LEVEL_WARN:
            return "warn ";
        case LOG_LEVEL_INFO:
            return "info ";
        case LOG_LEVEL_DEBUG:
            return "debug";
        default:
            return "unknown";
    }
}

static inline int PURE
log_should_log(const log_level_t level)
{
    assert(level != LOG_LEVEL_NONE);
    return (level <= log_level);
}

void NONNULL PRINTF(3,4)
log_print_real(const int has_errno,
               const log_level_t level,
               const char * const fmt, ...)
{
    if (!log_should_log(level) || log_quiet) {
        return;
    }

    const char * err = NULL;
    if (has_errno) {
        err = strerror(errno);
    }

    const char * const levstr = log_level_to_string(level);
    fputs(PACKAGE ": ", stderr);
    fputs(levstr, stderr);
    fputs(": ", stderr);

    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    if (err != NULL) {
        fprintf(stderr, ": %c%s\n", tolower(err[0]), err+1);
    } else {
        fputc('\n', stderr);
    }

    return;
}
