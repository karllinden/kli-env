/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "common.h"
#include "env.h"
#include "log.h"

int
main(int argc, char ** argv)
{
    int exit_status = EXIT_SUCCESS;

    int remove = 0;

    env_t env;

    const gop_option_t options[] = {
        {"remove", 'r', GOP_NONE, &remove, NULL,
            "remove installation instead of installing", NULL},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_usage(gop, "[OPTION...] [ROOT]");
    gop_description(gop, "Install an environment into ROOT.\n"
                         "\n"
                         "Without ROOT environment will be installed "
                         "into ${HOME}/%s.", PACKAGE);
    gop_add_table(gop, "Program options:", options);
    common_gop(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (argc > 2) {
        log_error("invalid number of arguments");
        return EXIT_FAILURE;
    }

    log_set_level(LOG_LEVEL_WARN);
    if (!remove) {
        env_init(&env);
        if (env_create(&env, argv[1])) {
            exit_status = EXIT_FAILURE;
        }
        env_deinit(&env);
    } else {
        if (env_remove(argv[1])) {
            exit_status = EXIT_FAILURE;
        }
    }

    return exit_status;
}
