/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "common.h"
#include "env.h"
#include "log.h"

static int
prefix_print_iter(env_t * env,
                  env_prefix_t * prefix,
                  void * data)
{
    unsigned int * countp = data;
    (*countp)++;
    env_prefix_print(env, prefix, *countp, stdout);
    return 0;
}

int
main(int argc, char ** argv)
{
    int exit_status = EXIT_FAILURE;

    int          first     = 0;
    int          last      = 0;
    const char * before    = NULL;
    const char * after     = NULL;

    int          create    = 0;
    int          remove    = 0;
    int          move      = 0;
    const char * swap      = NULL;
    int          show      = 0;
    int          enable    = 0;
    int          disable   = 0;
    int          export    = 0;
    int          unexport  = 0;

    int          install   = -1;
    int          aclocal   = -1;
    int          bins      = -1;
    int          sbins     = -1;
    int          libs      = -1;
    int          pkgconfig = -1;

    const char * root      = NULL;

    /* set to 1 if location has been set */
    int          location  = 0;

    /* set to 1 if a modifier was given */
    int          modifier  = 0;

    /* set to the name of the other prefix to lookup, if any */
    const char * oname     = NULL;

    env_t env;
    env_init(&env);

    env_prefix_t * prefix = NULL;
    env_prefix_t * other  = NULL;

    const gop_option_t actions[] = {
        {"create", 'c', GOP_NONE, &create, NULL, "create PREFIX", NULL},
        {"remove", 'r', GOP_NONE, &remove, NULL,
            "remove PREFIX", NULL},
        {"move", 'm', GOP_NONE, &move, NULL,
            "move PREFIX to a different position; needs to be "
            "accompanied with a location", NULL},
        {"swap", 'w', GOP_STRING, &swap, NULL,
            "swap positions between PREFIX and SWAP", "SWAP"},
        {"show", 's', GOP_NONE, &show, NULL,
            "show current configuration [default]"},
        {"enable", 'e', GOP_NONE, &enable, NULL, "enable PREFIX", NULL},
        {"disable", 'd', GOP_NONE, &disable, NULL, "disable PREFIX",
            NULL},
        {"export", 'x', GOP_NONE, &export, NULL,
            "export the PREFIX to bashrc", NULL},
        {"unexport", 'u', GOP_NONE, &unexport, NULL,
            "do not export the PREFIX to bashrc", NULL},
        GOP_TABLEEND
    };

    const gop_option_t locations[] = {
        {"first", 'f', GOP_NONE, &first, NULL,
            "when used with --create, create the prefix at the first "
            "position (highest priority) [default]; when used with "
            "--move, move the prefix to the first position"},
        {"last", 'l', GOP_NONE, &last, NULL,
            "when used with --create, create the prefix at the last "
            "position (lowest priority); when used with --move, move "
            "the prefix to the last position"},
        {"before", 'b', GOP_STRING, &before, NULL,
            "when used with --create, create the prefix at the "
            "position before AFTER; when used with --move, move "
            "the prefix to the position before AFTER", "AFTER"},
        {"after", 'a', GOP_STRING, &after, NULL,
            "when used with --create, create the prefix at the "
            "position after BEORE; when used with --move, move "
            "the prefix to the position after BEFORE", "BEFORE"},
        GOP_TABLEEND
    };

    const gop_option_t modifiers[] = {
        {"install", 'I', GOP_YESNO, &install, NULL,
            "set whether PREFIX is marked for installation", NULL},
        {"aclocal", 'A', GOP_YESNO, &aclocal, NULL,
            "set whether aclocal should be used from PREFIX", NULL},
        {"bins", 'B', GOP_YESNO, &bins, NULL,
            "set whether bins should be used from PREFIX", NULL},
        {"sbins", 'S', GOP_YESNO, &sbins, NULL,
            "set whether sbins should be used from PREFIX", NULL},
        {"libs", 'L', GOP_YESNO, &libs, NULL,
            "set whether libs should be used from PREFIX", NULL},
        {"pkgconfig", 'P', GOP_YESNO, &pkgconfig, NULL,
            "set whether pkgconfig should be used from PREFIX", NULL},
        GOP_TABLEEND
    };

    const gop_option_t misc[] = {
        {"root", 'R', GOP_STRING, &root, NULL,
            "use environment root ROOT", "ROOT"},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_usage(gop, "[OPTION...] [PREFIX]");
    gop_description(gop, "Configure PREFIX in %s\n"
                         "\n"
                         "Without PREFIX the current configuration "
                         "will be shown.", PACKAGE);
    gop_add_table(gop, "Actions:", actions);
    gop_add_table(gop, "Locations:", locations);
    gop_add_table(gop, "Modifiers:", modifiers);
    gop_add_table(gop, "Miscellaneous options:", misc);
    common_gop(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (argc > 2) {
        log_error("invalid number of arguments");
        return EXIT_FAILURE;
    }

    location = first + last + (before != NULL ? 1 : 0) +
               (after != NULL ? 1 : 0);
    if (location > 1) {
        log_error("only one of --first, --last, --before and --after "
                  "may be given at the same time");
        return EXIT_FAILURE;
    }

    if (install != -1 || aclocal != -1 || bins != -1 || sbins != -1 ||
        libs != -1 || pkgconfig != -1)
    {
        modifier = 1;
    }

    if ((create && remove) ||
        (enable && disable) ||
        (export && unexport))
    {
        log_error("contradictory actions");
        return EXIT_FAILURE;
    } else if (create && (move || swap)) {
        log_error("--create may not be combined with --move or "
                  "--swap");
        return EXIT_FAILURE;
    } else if (remove &&
               (create || move || swap || enable || disable || export ||
                unexport))
    {
        log_error("--remove may not be combined with any other "
                  "action except --show");
        return EXIT_FAILURE;
    } else if (remove && modifier) {
        log_error("--remove may not be combined with any modifier");
        return EXIT_FAILURE;
    } else if (move && swap) {
        log_error("--move and --swap may not be given at the same "
                  "time");
        return EXIT_FAILURE;
    } else if (move && !location) {
        log_error("--move needs a location");
        return EXIT_FAILURE;
    } else if (!create && !move && location) {
        log_error("location was specified but neither was --create nor "
                  "--move");
        return EXIT_FAILURE;
    } else if (argc <= 1 &&
               (create || remove || move || swap || enable || disable ||
                export || unexport))
    {
        log_error("specified action requires PREFIX to be given");
        return EXIT_FAILURE;
    } else if (argc <= 1 && modifier) {
        log_error("modifiers requires PREFIX to be given");
        return EXIT_FAILURE;
    }

    /* set default action to show if none is given, also do not show if
     * only a modifier is given */
    if (create + remove + move + swap + show + enable + disable +
        export + unexport + modifier == 0)
    {
        show = 1;
    }

    /* default to creating prefix at first position */
    if (create && !location) {
        first = 1;
        location = 1;
    }

    log_set_level(LOG_LEVEL_INFO);

    /* should the prefix be looked up? */
    if (argc == 2) {
        env_prefix_get(&env, argv[1], &prefix);
    }

    if (before != NULL) {
        oname = before;
    } else if (after != NULL) {
        oname = after;
    } else if (swap != NULL) {
        oname = swap;
    }
    if (oname != NULL) {
        env_prefix_get(&env, oname, &other);
    }

    if (env_open(&env, root)) {
        goto error;
    }
    if (env_load(&env)) {
        goto error;
    }

    if (create && prefix != NULL) {
        log_error("prefix %s already exists", argv[1]);
        goto error;
    } else if (!create && argc == 2 && prefix == NULL) {
        log_error("could not find prefix %s", argv[1]);
        goto error;
    } else if (oname != NULL && other == NULL) {
        log_error("could not find prefix %s", oname);
    }

    if (create) {
        if (prefix != NULL) {
            ;
        }

        if (first) {
            prefix = env_prefix_new_most(&env, argv[1]);
        } else {
            prefix = env_prefix_new_least(&env, argv[1]);
        }
        if (prefix == NULL) {
            goto error;
        }
        if (before != NULL) {
            env_prefix_move_before(&env, prefix, other);
        } else if (after != NULL) {
            env_prefix_move_after(&env, prefix, other);
        }
        if (env_prefix_create(&env, prefix)) {
            goto error;
        }
    } else if (remove) {
        if (env_prefix_remove(&env, prefix)) {
            goto error;
        }
        env_prefix_destroy(&env, prefix);
    } else if (move) {
        if (first) {
            env_prefix_move_first(&env, prefix);
        } else if (last) {
            env_prefix_move_last(&env, prefix);
        } else if (before != NULL) {
            env_prefix_move_before(&env, prefix, other);
        } else /* if (after != NULL) */ {
            env_prefix_move_after(&env, prefix, other);
        }
    } else if (swap) {
        env_prefix_swap(&env, prefix, other);
    }

    if (enable) {
        env_prefix_ynq(&env, prefix, ENV_PREFIX_ENABLE, ENV_YES);
    } else if (disable) {
        env_prefix_ynq(&env, prefix, ENV_PREFIX_ENABLE, ENV_NO);
    }

    if (export) {
        env_prefix_ynq(&env, prefix, ENV_PREFIX_EXPORT, ENV_YES);
    } else if (unexport) {
        env_prefix_ynq(&env, prefix, ENV_PREFIX_EXPORT, ENV_NO);
    }

    const struct {
        int option;
        int flag;
    } mods[] = {
        {install,   ENV_PREFIX_INSTALL},
        {aclocal,   ENV_PREFIX_ACLOCAL},
        {bins,      ENV_PREFIX_BINS},
        {sbins,     ENV_PREFIX_SBINS},
        {libs,      ENV_PREFIX_LIBS},
        {pkgconfig, ENV_PREFIX_PKGCONFIG}
    };
    for (unsigned int i = 0; i < ARRAY_SIZE(mods); ++i) {
        if (mods[i].option == 1) {
            env_prefix_ynq(&env, prefix, mods[i].flag, ENV_YES);
        } else if (mods[i].option == 0) {
            env_prefix_ynq(&env, prefix, mods[i].flag, ENV_NO);
        }
    }

    /* show must go last, or all changes will not show up! */
    if (show) {
        if (argc == 1) {
            /* show all */
            unsigned int count = 0;
            env_iter_forward(&env, &prefix_print_iter, &count);
            if (count == 0) {
                puts("no prefixes");
            }
        } else {
            /* show only prefix */
            env_prefix_print(&env, prefix, 0, stdout);
        }
    }

    if (env_save(&env)) {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    env_close(&env);
    env_deinit(&env);
    return exit_status;
}
