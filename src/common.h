/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2014-2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef COMMON_H
# define COMMON_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <gop.h>

/* non function like */
# define COLD               __attribute__((cold))
# define CONST              __attribute__((const))
# define NONNULL            __attribute__((nonnull))
# define PURE               __attribute__((pure))
# define RETURNS_NONNULL    __attribute__((returns_nonnull))
# define UNUSED             __attribute__((unused))
# define WARN_UNUSED_RESULT __attribute__((warn_unused_result))

/* function like */
# define NONNULLA(...)      __attribute__((nonnull(__VA_ARGS__)))
# define PRINTF(si, ftc)    __attribute__((format(printf,si,ftc)))

# define ARRAY_SIZE(array)  (sizeof(array)/sizeof(array[0]))

int common_gop(gop_t * const gop) NONNULL;

#endif /* !COMMON_H */
