/*
 * This file is part of kli-env.
 *
 * Copyright (C) 2014-2015, 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef LOG_H
# define LOG_H 1

# include <stdarg.h>
# include <stdint.h>

# include "common.h"

# define log_print(...)       log_print_real(0, __VA_ARGS__)
# define log_print_errno(...) log_print_real(1, __VA_ARGS__)

# define log_fatal(...)       log_print(LOG_LEVEL_FATAL, __VA_ARGS__)
# define log_error(...)       log_print(LOG_LEVEL_ERROR, __VA_ARGS__)
# define log_warn(...)        log_print(LOG_LEVEL_WARN,  __VA_ARGS__)
# define log_info(...)        log_print(LOG_LEVEL_INFO,  __VA_ARGS__)
# define log_debug(...)       log_print(LOG_LEVEL_DEBUG, __VA_ARGS__)

# define log_fatal_errno(...) log_print_errno(LOG_LEVEL_FATAL, __VA_ARGS__)
# define log_error_errno(...) log_print_errno(LOG_LEVEL_ERROR, __VA_ARGS__)
# define log_warn_errno(...)  log_print_errno(LOG_LEVEL_WARN, __VA_ARGS__)
# define log_info_errno(...)  log_print_errno(LOG_LEVEL_INFO, __VA_ARGS__)
# define log_debug_errno(...) log_print_errno(LOG_LEVEL_DEBUG, __VA_ARGS__)

# define LOG_LEVEL_NONE  0
# define LOG_LEVEL_FATAL 1
# define LOG_LEVEL_ERROR 2
# define LOG_LEVEL_WARN  3
# define LOG_LEVEL_INFO  4
# define LOG_LEVEL_DEBUG 5
typedef uint8_t log_level_t;

void                log_set_level         (const log_level_t level);
void                log_set_quiet         (const int quiet);

log_level_t         log_level_from_string (const char * string)         NONNULL PURE;

void                log_print_real        (const int has_errno,
                                           const log_level_t level,
                                           const char * const fmt, ...) NONNULL PRINTF(3,4);


#endif /* !LOG_H */
