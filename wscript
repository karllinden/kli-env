#!/usr/bin/env python
# encoding: utf-8

import os
import sys

from waflib import Logs, Options, TaskGen

VERSION='7'
APPNAME='kli-env'
HOMEPAGE='https://gitlab.com/karllinden/' + APPNAME
BUGREPORT=HOMEPAGE + '/issues'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
g_maxlen = 40
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')
    opt.add_option('--enable-assert', action='store_true',
                   default=False, help='enable assertions')
    opt.add_option('--debug', action='store_true', default=False,
                   help='do a debug build')
    opt.add_option('--no-optimize', action='store_true', default=False,
                   help='disable optimizations')

def configure(conf):
    conf.load('compiler_c')
    conf.load('gnu_dirs')

    conf.env.append_unique('CFLAGS', '-std=gnu11')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    if conf.options.debug:
        conf.options.no_optimize = True
        conf.options.enable_assert = True
        conf.env.append_unique('CFLAGS', '-ggdb')
    if not conf.options.enable_assert:
        conf.define('NDEBUG', 1)
    if not conf.options.no_optimize:
        conf.env.append_unique('CFLAGS', ['-O3', '-funroll-loops'])

    conf.define('_GNU_SOURCE', 1)
    conf.define('PACKAGE', APPNAME)
    conf.define('VERSION', VERSION)
    conf.define('BUGREPORT', BUGREPORT)
    conf.define('HOMEPAGE', HOMEPAGE)

    conf.check_cfg(package='gop-4', uselib_store='GOP',
                   args='--cflags --libs')

    conf.write_config_header('config.h')

@TaskGen.feature('c')
@TaskGen.after_method('apply_incpaths')
def insert_blddir(self):
    self.env.prepend_value('INCPATHS', self.bld.bldnode.abspath())

def build_tool(bld, name):
    bld.program(
        source       = ['src/%s.c' % name, 'src/common.c'],
        defines      = 'PROGRAM_NAME=\"kli-%s\"' % name,
        target       = 'ke-' + name,
        use          = ['ke', 'GOP'],
        install_path = bld.env['BINDIR']
    )

def build(bld):
    bld.env.append_unique('CFLAGS', '-DHAVE_CONFIG_H=1')

    bld.stlib(
        source       = [
            'src/env.c',
            'src/log.c',
        ],
        target       = 'ke',
        use          = ['GOP'],
        install_path = False
    )
    build_tool(bld, 'cmake')
    build_tool(bld, 'config')
    build_tool(bld, 'configure')
    build_tool(bld, 'run')
    build_tool(bld, 'setup')
    build_tool(bld, 'waf')
